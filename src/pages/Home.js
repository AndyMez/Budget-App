import axios from "axios";
import { useEffect, useState } from "react";
import { FormOperation } from "../components/FormOperation";
import { Month } from "../components/Month";
import { Transactions } from "../components/Transactions";
import "../Home.css";

export function Home() {
  const [transactions, setTransactions] = useState([]);

  async function fetchTransactions() {
    const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget');
    setTransactions(response.data);
  }

  async function fetchMonth(month) {
    const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/month/' + month);
    setTransactions(response.data);
  }

  async function deleteTransaction(id) {
    await axios.delete(process.env.REACT_APP_SERVER_URL+'/api/budget/' + id);
    setTransactions(transactions.filter((item) => item.id !== id));
  }

  async function addTransaction(transaction) {
    const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/budget', transaction);
    setTransactions([...transactions, response.data]);
  }

  useEffect(() => {
    fetchTransactions();
  }, []);

  function TotalPrice() {
    let total = 0;
    for (const element of transactions) {
      total += element.amount;
    }
    return total;
  }

  return (
    <div className="body">
      <div className="container">
        <section className="main">
          <h1 className="title">Comptes</h1>

          <div className="current-sold">
            <h2>Solde actuel</h2>
            <p>{Number.parseFloat(TotalPrice()).toFixed(2)}</p>
          </div>

          <Month reset={fetchTransactions} selectMonth={fetchMonth} />

          <div className="recent-operation">
            <h2>Opérations récentes :</h2>

            {transactions.map((item) => (
              <Transactions
                key={item.id}
                transaction={item}
                onDelete={deleteTransaction}
              />
            ))}
          </div>

          <FormOperation onFormSubmit={addTransaction} />
        </section>
      </div>
    </div>
  );
}
