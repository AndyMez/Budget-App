import { useState } from "react";


const initialState = {
    amount: '',
    category: '',
    comment: '',
    date: ''
}


export function FormOperation({onFormSubmit, transaction = initialState}) {

    const [form, setForm] = useState(transaction);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return(
        <form onSubmit={handleSubmit} className="form-transaction">
            <div>                
                <label for="amount">Montant</label>
                <input required type="number" name="amount" id="amount" onChange={handleChange}  />
            </div>
            <div>
                <label for="category">Categorie</label>
                <input type="text" name="category" id="category" onChange={handleChange}  />
            </div>
            <div>                
                <label for="comment">Commentaire</label>
                <input type="text" name="comment" id="comment" onChange={handleChange} />
            </div>
            <div>                
                <label for="date">Date</label>
                <input required type="date" name="date" id="date" onChange={handleChange} />
            </div>
            <button className="btnSubmit">Submit</button>
        </form>
    );
}