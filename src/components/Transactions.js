import'../Home.css'


export function Transactions ({transaction, onDelete}) {

    return (
        <div className="operation-page">
            <div className="operation">     
                <div className="operation-details">
                    <span>{Number.parseFloat(transaction.amount).toFixed(2)}</span>
                    <span>{transaction.category}</span>
                    <span>{transaction.comment}</span>
                    <span>{transaction.date.substring(0, transaction.date.length - 14)}</span>
                    <div className="div-btn">
                        <button onClick={() => onDelete(transaction.id)} className="removeBtn">X</button>
                    </div>
                </div>
            </div>          
        </div>
    )
}