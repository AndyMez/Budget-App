# Projet n°5 : Projet budget

Pour ce cinquième projet, j'ai du réaliser une application de gestion de budget.

Projet : Individuel

Durée : 2 semaines

## Langage utilisé :

- React (Front)

- Node.js (Back)

## Objectif et consignes :

Créer une application de gestion de budget en respectant certaines consignes :

- Indiqués les dépenses (et les entrées) d'argent

- La catégorie de la dépense/entrée

- Facultatif : Un commentaire sur l'opération

- Afficher la balance totale

- Consulter les dépenses/entrées par mois 

- Consulter, ajouter, supprimer une opération

- Indiquer sa catégorie

- Responsive

## Organisation

Pour ce projet j'ai simplement commencé par le back en créant ma base de données suivies de mes connections au serveur, mon controller et mon repository.
Pour mon front j'ai créé ma Page ainsi que mes divers Components dont j'avais besoin pour ce projet
Je me suis principalement organisé à l'aide du Readme à ma disposition et du diagramme d'Use Case
[Lien du projet](https://andy-budget-app.netlify.app/)
![alt text](public/assets/budget-use-case.png)